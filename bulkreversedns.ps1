$doods = Import-Csv -Path $args[0] -Header addresses | Select-Object -Unique addresses
$reversedns = {
    param($dood)
    try{
        $name = [System.Net.Dns]::GetHostEntry($dood).HostName
        $output = $dood + " - " + $name
        $output
    }
    catch{
        $output = "An error occured resolving: " + $dood
        $output
    }
}

$MaxThreads = 100
$RunspacePool = [runspacefactory]::CreateRunspacePool(1, $MaxThreads)
$RunspacePool.Open()
$Jobs = @()
$results = @()

foreach ($dood in $doods.addresses){
    $PowerShell = [powershell]::Create()
	$PowerShell.RunspacePool = $RunspacePool
	$PowerShell.AddScript($reversedns).AddArgument($dood)
    $Jobs += [PSCustomObject]@{ Pipe = $PowerShell; Status = $PowerShell.BeginInvoke() }
}

while ($Jobs.IsCompleted -contains $false) {
	Start-Sleep 1
}

foreach ($runspace in $Jobs ) {
    $results += $runspace.Pipe.EndInvoke($runspace.Status)
    $runspace.Pipe.Dispose()
}

$pool.Close() 
$pool.Dispose()

foreach ($result in $results){
    Write-Output $result
}